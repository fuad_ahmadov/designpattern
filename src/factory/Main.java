package factory;

public class Main {
    public static void main(String[] args) {
        AnimalFactory animalFactory = new AnimalFactory();

        Animal cat = animalFactory.getCar(AnimalEnum.CAT);
        if (cat != null)
            System.out.println("Cat's name : " + cat.name());

        Animal dog= animalFactory.getCar(AnimalEnum.DOG);
        if (dog != null)
            System.out.println("Dog's name : " + dog.name());
    }

}
