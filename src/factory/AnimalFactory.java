package factory;


public class AnimalFactory {

    public Animal getCar(AnimalEnum animalEnum) {

        Animal animal = null;

        switch (animalEnum) {
            case CAT:
                animal = new Cat();
                break;

            case DOG:
                animal = new Dog();
                break;
        }

        return animal;
    }
}
