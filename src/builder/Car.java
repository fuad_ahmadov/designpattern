package builder;

public class Car {
    private String name;
    private String color;

    private Car(Builder builder) {
        name = builder.name;
        color = builder.color;
    }

    public static class Builder {

        private String name;
        private String color;

        public Builder() {
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder color(String color) {
            this.color = color;
            return this;
        }

        public Car build() {
            return new Car(this);
        }
    }

}