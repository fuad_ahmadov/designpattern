package builder;

public class Main {
    public static void main(String[] args) {
        Car car = new Car.Builder()
                .name("Bmw")
                .color("black")
                .build();
    }
}
