package prototype;

import java.util.List;

public class Main {
    public static void main(String[] args) throws CloneNotSupportedException {

        Animal animal = new Animal();
        animal.addAnimal();

        Animal newAnimal = (Animal) animal.clone();
        List<String> list = newAnimal.getAnimals();
        list.add("Lion");
        System.out.println("yeniUye List: " + list);

    }
}
