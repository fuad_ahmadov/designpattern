package prototype;

import java.util.ArrayList;
import java.util.List;

public class Animal implements Cloneable {

    private List<String> animals;

    public Animal() {
        animals = new ArrayList<String>();
    }

    public Animal(List<String> liste) {
        this.animals = liste;
    }

    public void addAnimal() {

        animals.add("Monkey");
        animals.add("Dog");
        animals.add("Cat");
    }

    public List<String> getAnimals() {
        return animals;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
//        Object clone = super.clone();
        List<String> animals = new ArrayList<String>();
        for (String s : this.getAnimals()) {
            animals.add(s);
        }
        return new Animal(animals);
    }
}
